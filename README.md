
# Drupal 9.3 HMCPL Intercept profile (no Pantheon).

## Clone the project

```
$ git clone git@gitlab.com:codejourneymen/hmcpl.git hmcpl_intercept
$ cd hmcpl_intercept
```

## Initialize the project

1. Run the following commands:
    ```
    $ lando start
    $ lando composer install
    ```

2. Ensure the profile `intercept_profile` is installed with the path `web/profiles/contrib/intercept_profile`
3. Ensure the library `jquery-ui-touch-punch` is installed with the path `/web/libraries/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js`
4. Ensure the library `dropzone` is installed with the path `web/libraries/dropzone/dist/min/dropzone.min.js`

## Import the database (DB)

Import your DB with `lando`

Example:
```
$ lando db-import backups/database.sql
```

## Download site files

1. Download the public site files to `web/sites/default/files/`
2. Download the private site files to `private/`
3. Link the public site files to the root directory:
    ```
    $ ln -s web/sites/default/files files
    ```

## Update site settings

Add specific `lando` settings
```
$ cp settings.lando.php web/sites/default
$ cd web/sites/default
$ cp ../example.settings.local.php ./settings.php
$ cat ../../../hmcpl.settings.php >> settings.php
```

## Finish site setup and login

```
$ lando drush cr
$ lando drush cim -y
$ lando drush cr
$ lando drush updb -y
$ lando drush uli --uri=https://interceptd9.lndo.site
```
