/**
 * HMCPL Defaults
 */
$settings['hash_salt'] = 'idFTwuoSe5D9wPQm76llKfC3YyXRE5U84n-FnTZn4WvjCYDKCx6AeWw8LT7fdhej1Ki-sAZ8pQ';

/**
 * Live Environment - this is default.
 * Override in settings.local.php for stage/local development
 */
$config['config_split.config_split.development']['status'] = FALSE;
$config['environment_indicator.indicator']['bg_color'] = '#961717';
$config['environment_indicator.indicator']['fg_color'] = '#ffffff';
$config['environment_indicator.indicator']['name'] = 'Live';
$settings['config_sync_directory'] = '../config/sync';
$settings['file_private_path'] = '../private';

/**
 * Load local development override configuration, if available.
 *
 * Create a settings.local.php file to override variables on secondary (staging,
 * development, etc.) installations of this site.
 *
 * Typical uses of settings.local.php include:
 * - Disabling caching.
 * - Disabling JavaScript/CSS compression.
 * - Rerouting outgoing emails.
 *
 * Keep this code block at the end of this file to take full effect.
 */
#
if (file_exists($app_root . '/' . $site_path . '/settings.local.php')) {
  include $app_root . '/' . $site_path . '/settings.local.php';
}

# Lando
if (file_exists($app_root . '/' . $site_path . '/settings.lando.php')) {
  include $app_root . '/' . $site_path . '/settings.lando.php';
}
