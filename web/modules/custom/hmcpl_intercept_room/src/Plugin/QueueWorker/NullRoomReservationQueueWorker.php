<?php 
/**
 * @file
 * Contains \Drupal\hmcpl_intercept_room\Plugin\QueueWorker\NullRoomReservationQueueWorker.
 */

namespace Drupal\hmcpl_intercept_room\Plugin\QueueWorker;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\intercept_room_reservation\Entity\RoomReservation;

/**
 * Processes cleanup for room reservations with a null parent entity (field_room).
 *
 * @QueueWorker(
 *   id = "null_room_reservations",
 *   title = @Translation("Null Room Reservation Queue Worker"),
 *   cron = {"time" = 60}
 * )
 */
class NullRoomReservationQueueWorker extends QueueWorkerBase {

  /**
   * 
   * {@inheritdoc}
   */
  public function processItem($item) {
    if ($item instanceof RoomReservation && empty($item->getParentEntity())) {
      $item->delete();
    }
  }

}